;;;; Cl-who Renderer for Coleslaw
;;;; Taken from: https://gitlab.common-lisp.net/dkochmanski/sclp/blob/ca3ca168d1d062b78be9ef249539c18acef48d9a/plugins/cl-who-renderer.lisp
;;;; Copyright 2016 Daniel Kochmański License unknown but obviously
;;;; meant for use by others.

(eval-when (:compile-toplevel :load-toplevel)
  (asdf:load-system 'cl-who))

(defpackage :coleslaw-cl-who-renderer
  (:use #:cl #:cl-who)
  (:import-from #:coleslaw :render-text)
  (:export #:enable))

(in-package :coleslaw-cl-who-renderer)

(defmethod render-text (text (format (eql :cl-who)))
  (let* ((*package* (find-package '#:coleslaw-cl-who-renderer))
         (sexps (with-input-from-string (v text)
                  (do* ((line (read v)
                              (read v nil 'done))
                        (acc (list line)
                             (cons line acc)))
                       ((eql line 'done)
                        (nreverse (cdr acc)))))))
    (eval `(with-html-output-to-string (v) ,@sexps))))

(defun enable ())
