;;;;;
title: January 2016 Lisp Game Jam Post Mortem
excerpt: A description of my experience developing for the jam.
tags: game-jam, lisp, games
date: 2016-01-14 05:00:00
author: Lily Carpenter
format: md
;;;;;

#### Updates
* 2017-05-15T00:10:00Z Fix various broken links.

See [The game jam page](http://itch.io/jam/january-2016-lisp-game-jam) for context.

### My experience

I decided this year after having hung out in #lispgames and piddled around with trying (mostly unsuccessfully) to learn OpenGL for a couple months to actually build something for the first #lispgames (a channel on [Freenode IRC](http://freenode.net)) game jam.

This was overall a pleasant experience, I learned a lot and ended up making a game that you can find on [my gitlab](https://gitlab.com/azrazalea/crypts-and-corpses). I consider this game to be a spiritual prequel to my eventual large game project in a lot of ways, since the theme is similar but the scope massively small. I had a lot of fun and learned a lot of things, and I plan on writing about them here in length, but i'll start with the post mortem template given by the game jam organizers.

#### What dialect/tools/libraries did you use?

I used Common Lisp. I programmed in Emacs/SLY (not slime, and also not the scheme engine sly) and used a C terminal (with corresponding wrapper I put together) called [BearLibTerminal](http://foo.wyrd.name/en:bearlibterminal). The wrapper I wrote can be found at [gitlab](https://gitlab.com/azrazalea/cl-bearlibterminal). I used [cl-autowrap](https://github.com/rpav/cl-autowrap) and [c2ffi](https://github.com/rpav/c2ffi) to make this wrapper. For the actual game I also used axion's [crawler](http://git.axity.net/axion/crawler) for dungeon generation and [split-sequence](https://gitlab.common-lisp.net/xcvb/split-sequence), [alexandria](https://gitlab.common-lisp.net/alexandria/alexandria), and [cl-heap](https://github.com/TheRiver/CL-HEAP) as supporting utility libraries. Finally I use [quicklisp](https://beta.quicklisp.org) of course to get the libraries and [SBCL](http://sbcl.org/) as my development Lisp implementation. I also tested on [CCL](http://clozure.com/clozure-cl.html) and tried (but failed) to test on [Clisp](http://clisp.org/).

#### What sort of game did you choose to make, and why?

I chose to make a small necromancer themed roguelike. The main reason for this was that I was able to get away with using BearLibTerminal due to the minimal graphics requirements and therefore could avoid trying to learn OpenGL during the jam (since I still haven't learned it properly). The secondary reason is that it is a nice prototype for some of my concepts about my [forever project game](https://azrazalea.net/posts/A-ramble-about-a-game-idea.html).

#### What went right, what were some successes?

BearLibTerminal worked out great, besides some problems people are having currently getting it to work on their machines. It was easy to work with, I didn't run into any bugs, and it mostly stayed out of the way. Using Common Lisp worked out great as well, I strongly enjoyed my first real experience with CLOS and mixing it with functional programming and while I didn't always take proper advantage the REPL driven development worked out great. I thought overall the vision came together decently as well, though not perfectly.

#### What went wrong?

A few things. First off, as expected, even with my vastly downsized requirements I ended up having to drop features in order to get it done. This means I ended up with a pretty bare bones/boring game, but a working one. While I already "knew" this, I found concrete evidence that making a game fun can be hard. I don't think the game is currently fun, despite all the effort put into it. It largely feels incomplete, too simple.

Another thing that I wanted to do but didn't get done was adding unit and property tests to the game. I still plan on doing this post jam, but so far there are absolutely no tests.

Finally the code quality was tremendously awful. It is all one file and not organized at all, and a large part of that is due to feeling rushed. I should have planned my time usage better and made less excuses about how it was fine to write bad code now cause I could rewrite it after the jam. It bit me more than once during the jam, and I had to refactor things to get them to work at all.

Overall I feel the biggest things that went wrong were overestimating my motivation and underestimating difficultly of things.

#### What did you learn?

I learned CLOS is a joy to work with, and a ton of specific things about it.

I learned about [Dijkstra maps](http://www.roguebasin.com/index.php?title=Dijkstra) and all the cool things you can do with them. I also learned all over again how sometimes I can just get a mental block on something and then have it just suddenly click (in this case how to flood fill the map with dijkstra map data).

I learned to restart my lisp image often, because otherwise you can/will have lingering bugs that you don't know exist (this bit me only once, luckily).

I learned that the simple things in AI can be both surprisingly hard and simple.

I learned that feeling rushed is not overly good for code quality (I already knew this, but it was reinforced.)

Finally I learned once again that I tend to overestimate my motivation and drive to complete a project.

#### What did Lisp enable you to do well in this entry?

A big thing was that the little motivation I did have to work on this largely came from getting to work with Lisp. Otherwise I probably would have just dropped out of the jam. Besides that I'm not sure that it really gave me many specific advantages, as I didn't really use the language to its full potential. However, I feel the ability to use CLOS + functional programming was very beneficial as well as the ease of REPL driven development (especially since I ended up not writing tests) and the general dynamic nature of lisp.

#### What challenges did Lisp present in making your entry happen?

Truthfully, not many and none major. I am still relatively unfamiliar with the language so I had some growing pains while learning, but #lisp and #lispgames were very helpful with those.
I'd say most challenges were just due to my ignorance more than with the language itself. I even started getting used to the debugger after all the errors I caused!

#### How long have you used lisp and what previous gamedev experience do you have?

I have used lisp really for only a couple to few months. I have played with it as long as two years ago but haven't really ever written anything complete until recently.

In a similar vein, I have never completed a game of my own design before. I have gone through a few books that guided you through creating games, mostly in my youth, but never done my own idea from start to finish.

### Conclusion

Overall this game jam was definitely a positive event. I learned a lot, got some good practice with Common Lisp, and even got something to show off (even with the awful state of its code). Definitely a win for me.

Another exciting thing was getting to see the other entries in the jam, I highly recommend checking them out. They are great. I'm pretty sure all of the games were more complete/fun than mine too!

I'm am looking forward to the next game jam, hopefully in a few months.
