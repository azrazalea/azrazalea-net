(ql:quickload :coleslaw)
(ql:quickload :hunchentoot)
(ql:quickload :cl-inotify) ; Had to apt-get install libfixposix-dev
(ql:quickload :iterate)
(ql:quickload :bordeaux-threads)

(defpackage :azrazalea-net
  (:use cl iterate cl-inotify bordeaux-threads)
  (:export build-coleslaw start-server stop-server deploy))

(in-package :azrazalea-net)

(defun build-coleslaw ()
  "Builds site with coleslaw. Makes the assumption that the directory the lisp image
   was started in is the site directory."
  (coleslaw:main *default-pathname-defaults*))

(defun watch-recursive (things)
  (iter
    (with inotify = (make-inotify))
    (with flags = '(:create :modify :delete))
    (for dir in things)
    (watch inotify dir flags)
    (iter (for subdir in (uiop:subdirectories dir))
      (watch inotify subdir flags))
    (finally (do-events (event inotify :blocking-p t)
               (build-coleslaw)))))

(let ((acceptor (make-instance 'hunchentoot:easy-acceptor :document-root (merge-pathnames "dist/") :port 4242))
      (file-things (mapc
                    (lambda (name) (merge-pathnames name))
                    '("pages/"
                      "static/"
                      "posts/"
                      "themes/"
                      ".coleslawrc")))
      (inotify-thread nil))
  (defun start-server ()
    "Starts a hunchentoot server that serves the site on port 4242"
    (when inotify-thread (destroy-thread inotify-thread))
    (setf inotify-thread (make-thread (lambda ()
                                        (watch-recursive file-things)) :name "inotify"))
    (hunchentoot:start acceptor))
  (defun stop-server ()
    "Stops a hunchentoot server that serves the site on port 4242"
    (when inotify-thread (destroy-thread inotify-thread))
    (setf inotify-thread nil)
    (hunchentoot:stop acceptor)))

(defun deploy ()
  (uiop:run-program "rsync -az --delete dist/ --exclude .well-known/ web-admin@azrazalea.net:www/"))
